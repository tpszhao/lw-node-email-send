var nodemailer = require("nodemailer");
var ejs = require("ejs");

const { user, pass, mailList } = require("./cred");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: { user, pass },
});

ejs.renderFile(
  "./lyncwork-march-newsletter.html",
  { name: "Stranger" },
  function (err, data) {
    const mailOptions = {
      from: user,
      to: mailList,
      subject: "Lyncwork March 2022 Newsletter",
      html: data,
    };
    if (err) {
      console.log(err);
    } else {
      transporter.sendMail(mailOptions, function (err, info) {
        if (err) {
          console.log(err);
        } else {
          console.log("Message sent: " + info.response);
        }
      });
    }
  }
);
